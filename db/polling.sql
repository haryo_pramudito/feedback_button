-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2017 at 10:01 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `polling`
--

-- --------------------------------------------------------

--
-- Table structure for table `divisi`
--

CREATE TABLE `divisi` (
  `id_divisi` int(11) NOT NULL,
  `nama_divisi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `divisi`
--

INSERT INTO `divisi` (`id_divisi`, `nama_divisi`) VALUES
(1, 'Pengukuran'),
(2, 'Penetapan Hak'),
(3, 'Pengecekan & Layanan Online'),
(4, 'Layanan Tanpa Surat Kuasa'),
(5, 'Balik Nama'),
(6, 'Pengakuan Hak dan Peningkatan Hak atas Tanah'),
(7, 'Hak Tanggungan dan Roya'),
(8, 'Penyerahan Sertifikat');

-- --------------------------------------------------------

--
-- Table structure for table `kepuasan`
--

CREATE TABLE `kepuasan` (
  `id_kepuasan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_response` int(11) NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `kepuasan`
--

INSERT INTO `kepuasan` (`id_kepuasan`, `id_user`, `id_response`, `datetime`) VALUES
(114, 2, 1, '0000-00-00 00:00:00'),
(115, 2, 3, '2017-06-21 03:22:28'),
(116, 2, 1, '2017-06-21 03:41:08'),
(117, 2, 2, '2017-06-21 03:41:11'),
(118, 2, 3, '2017-06-21 03:41:15'),
(119, 2, 4, '2017-06-21 03:41:19'),
(120, 3, 1, '2017-06-21 03:41:39'),
(121, 3, 2, '2017-06-21 03:41:42'),
(122, 3, 3, '2017-06-21 03:41:46'),
(123, 3, 4, '2017-06-21 03:41:49'),
(124, 4, 1, '2017-06-21 03:42:35'),
(125, 4, 2, '2017-06-21 03:42:42'),
(126, 4, 3, '2017-06-21 03:42:45'),
(127, 4, 4, '2017-06-21 03:42:49'),
(128, 7, 1, '2017-06-21 03:43:23'),
(129, 7, 2, '2017-06-21 03:43:30'),
(130, 7, 3, '2017-06-21 03:43:34'),
(131, 7, 4, '2017-06-21 03:43:38'),
(132, 8, 1, '2017-06-21 03:44:07'),
(133, 8, 2, '2017-06-21 03:44:11'),
(134, 8, 3, '2017-06-21 03:44:15'),
(135, 8, 4, '2017-06-21 03:44:18'),
(136, 9, 1, '2017-06-21 03:44:30'),
(137, 9, 2, '2017-06-21 03:44:34'),
(138, 9, 3, '2017-06-21 03:44:37'),
(139, 9, 4, '2017-06-21 03:44:40'),
(140, 10, 1, '2017-06-21 03:44:54'),
(141, 10, 2, '2017-06-21 03:44:58'),
(142, 10, 3, '2017-06-21 03:45:01'),
(143, 10, 4, '2017-06-21 03:45:05'),
(144, 6, 1, '2017-06-21 03:47:58'),
(145, 6, 2, '2017-06-21 03:48:01'),
(146, 6, 3, '2017-06-21 03:48:04'),
(147, 6, 4, '2017-06-21 03:48:08');

-- --------------------------------------------------------

--
-- Table structure for table `nama`
--

CREATE TABLE `nama` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nama`
--

INSERT INTO `nama` (`id`, `nama`) VALUES
(1, 'Codeigniter'),
(2, 'Zend'),
(3, 'kohana'),
(4, 'yii'),
(5, 'silex'),
(6, 'slim'),
(7, 'Symponhy'),
(8, 'laravel'),
(9, 'nette');

-- --------------------------------------------------------

--
-- Table structure for table `poll`
--

CREATE TABLE `poll` (
  `id` int(11) DEFAULT NULL,
  `id_fr` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poll`
--

INSERT INTO `poll` (`id`, `id_fr`) VALUES
(NULL, 4),
(NULL, 9),
(NULL, 0),
(NULL, 0),
(NULL, 4),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 8),
(NULL, 6),
(NULL, 6),
(NULL, 1),
(NULL, 1),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 9),
(NULL, 9),
(NULL, 9),
(NULL, 9),
(NULL, 9),
(NULL, 9),
(NULL, 9),
(NULL, 9),
(NULL, 9),
(NULL, 9),
(NULL, 9),
(NULL, 9),
(NULL, 9),
(NULL, 5),
(NULL, 6),
(NULL, 6),
(NULL, 7),
(NULL, 9),
(NULL, 0),
(NULL, 9),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 9),
(NULL, 9),
(NULL, 8),
(NULL, 8),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 0),
(NULL, 8),
(NULL, 8),
(NULL, 8),
(NULL, 1),
(NULL, 0),
(NULL, 1),
(NULL, 1),
(NULL, 1),
(NULL, 1),
(NULL, 1),
(NULL, 1),
(NULL, 1),
(NULL, 3),
(NULL, 3),
(NULL, 4),
(NULL, 1),
(NULL, 1),
(NULL, 1),
(NULL, 0),
(NULL, 3),
(NULL, 3),
(NULL, 3),
(NULL, 3),
(NULL, 3),
(NULL, 3),
(NULL, 3),
(NULL, 3),
(NULL, 3),
(NULL, 3),
(NULL, 4),
(NULL, 4),
(NULL, 4),
(NULL, 7),
(NULL, 7),
(NULL, 0),
(NULL, 0),
(NULL, 2),
(NULL, 0),
(NULL, 0),
(NULL, 1),
(NULL, 1),
(NULL, 4),
(NULL, 4),
(NULL, 4),
(NULL, 5),
(NULL, 4),
(NULL, 1),
(NULL, 2),
(NULL, 2),
(NULL, 2),
(NULL, 0),
(NULL, 9),
(NULL, 9),
(NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `response`
--

CREATE TABLE `response` (
  `id_response` int(11) NOT NULL,
  `response` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `response`
--

INSERT INTO `response` (`id_response`, `response`) VALUES
(1, 'Sangat Puas'),
(2, 'Puas'),
(3, 'Cukup'),
(4, 'Tidak Puas');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `id_divisi` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `level`, `id_divisi`, `username`, `password`) VALUES
(1, 0, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(2, 1, 1, 'loket1', 'c479efa5391ffd6532615543039ce411'),
(3, 1, 2, 'loket2', '0a69b232dfcc1fe90226e12e4a509f28'),
(4, 1, 3, 'loket3', '15d848a70e9bc0b628a895b7b2634914'),
(6, 1, 4, 'loket4', '249dc06efbc3e5e0e229cab73480347d'),
(7, 1, 5, 'loket5', '0a7f4a82326114f1ed52573e4b3220e8'),
(8, 1, 6, 'loket6', '3f5b2810ea07a826da08e9cceedb845b'),
(9, 1, 7, 'loket7', '86887025abd07e9484538a025b26fa73'),
(10, 1, 8, 'loket8', '8c5a52f4b187173b2114349c91aaf8a1');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_kepuasan_cs`
--
CREATE TABLE `v_kepuasan_cs` (
`id_cs` int(11)
,`nama_cs` varchar(50)
,`divisi` varchar(255)
,`sangat_puas` bigint(21)
,`puas` bigint(21)
,`cukup` bigint(21)
,`tidak_puas` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_kepuasan_div`
--
CREATE TABLE `v_kepuasan_div` (
`id_divisi` int(11)
,`divisi` varchar(255)
,`sangat_puas` bigint(21)
,`puas` bigint(21)
,`cukup` bigint(21)
,`tidak_puas` bigint(21)
,`datetime` datetime
);

-- --------------------------------------------------------

--
-- Structure for view `v_kepuasan_cs`
--
DROP TABLE IF EXISTS `v_kepuasan_cs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_kepuasan_cs`  AS  select `user`.`id_user` AS `id_cs`,`user`.`username` AS `nama_cs`,`divisi`.`nama_divisi` AS `divisi`,count(if((`kepuasan`.`id_response` = 1),`kepuasan`.`id_response`,NULL)) AS `sangat_puas`,count(if((`kepuasan`.`id_response` = 2),`kepuasan`.`id_response`,NULL)) AS `puas`,count(if((`kepuasan`.`id_response` = 3),`kepuasan`.`id_response`,NULL)) AS `cukup`,count(if((`kepuasan`.`id_response` = 4),`kepuasan`.`id_response`,NULL)) AS `tidak_puas` from (((`user` join `divisi` on((`user`.`id_divisi` = `divisi`.`id_divisi`))) join `kepuasan` on((`user`.`id_user` = `kepuasan`.`id_user`))) join `response` on((`kepuasan`.`id_response` = `response`.`id_response`))) group by `user`.`username` order by `divisi`.`nama_divisi` ;

-- --------------------------------------------------------

--
-- Structure for view `v_kepuasan_div`
--
DROP TABLE IF EXISTS `v_kepuasan_div`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_kepuasan_div`  AS  select `divisi`.`id_divisi` AS `id_divisi`,`divisi`.`nama_divisi` AS `divisi`,count(if((`kepuasan`.`id_response` = 1),`kepuasan`.`id_response`,NULL)) AS `sangat_puas`,count(if((`kepuasan`.`id_response` = 2),`kepuasan`.`id_response`,NULL)) AS `puas`,count(if((`kepuasan`.`id_response` = 3),`kepuasan`.`id_response`,NULL)) AS `cukup`,count(if((`kepuasan`.`id_response` = 4),`kepuasan`.`id_response`,NULL)) AS `tidak_puas`,`kepuasan`.`datetime` AS `datetime` from (((`user` join `divisi` on((`user`.`id_divisi` = `divisi`.`id_divisi`))) join `kepuasan` on((`user`.`id_user` = `kepuasan`.`id_user`))) join `response` on((`kepuasan`.`id_response` = `response`.`id_response`))) group by `user`.`id_divisi` order by `divisi`.`nama_divisi` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kepuasan`
--
ALTER TABLE `kepuasan`
  ADD PRIMARY KEY (`id_kepuasan`);

--
-- Indexes for table `nama`
--
ALTER TABLE `nama`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`id_response`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kepuasan`
--
ALTER TABLE `kepuasan`
  MODIFY `id_kepuasan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;
--
-- AUTO_INCREMENT for table `nama`
--
ALTER TABLE `nama`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
