<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct()
    {
    	parent::__construct();
    	$this->load->model('Data_model');
    	date_default_timezone_set('Asia/Jakarta');
    	hak_aksesadmin();
        chek_session();
    }
	public function index()
	{
		$data['user'] = $this->Data_model->getUser()->result();
		$data['divisi'] = $this->Data_model->getDivisi()->result();
		$this->template->load('template','v_admin',$data);
	}
	public function laporan(){
		$data['title'] = "Laporan per Divisi";
		$data['url'] = "/feedback_button/admin/lap_divisi";
		$data['divisi'] = $this->Data_model->getDivisi()->result();
		$data['kep_divisi']  = $this->Data_model->getdata();
		$this->load->view('v_laporan',$data);
	}
	public function lap_divisi(){
		$data['title'] = "Laporan per Divisi";
		$data['url'] = "/feedback_button/admin/lap_divisi";
		$data['divisi'] = $this->Data_model->getDivisi()->result();
		$data['kep_divisi']  = $this->Data_model->getdata();
		$this->template->load('template','v_lapdivisi',$data);
	}
	public function lap_divisipdf(){
		$data['title'] = "LAPORAN KEPUASAN SEMUA PERIODE";
		$data['kep_divisi']  = $this->Data_model->getdata();
		$this->load->view('v_lapdivisipdf',$data);
	}
	public function lap_divisiexcel($tglmulai=null,$tglselesai=null){
		if ($tglmulai!=null) {
			$data['title'] = "LAPORAN PERIODE ".tgl_indo($tglmulai)." sd ".tgl_indo($tglselesai);
			$data['kep_divisi']  = $this->Data_model->getdataperiode($tglmulai,$tglselesai);
			$this->load->view('v_lapdivisiexcel',$data);
			redirect('admin/lap_divisipdf','refresh');
		} else {
			$data['title'] = "LAPORAN KEPUASAN SEMUA PERIODE";
			$data['kep_divisi']  = $this->Data_model->getdata();
			$this->load->view('v_lapdivisiexcel',$data);
			redirect('admin/lap_divisipdf','refresh');
		}
		
		
	}
	public function lap_divisi_harian(){
		$data['title'] = "Laporan per Divisi Harian";
		$data['url'] = "/feedback_button/admin/lap_divisi_harian";
		$data['divisi'] = $this->Data_model->getDivisi()->result();
		$data['kep_divisi']  = $this->Data_model->getdataharian();
		$this->template->load('template','v_lapdivisi',$data);
	}
	public function lap_divisiexcelharian(){
		
			$data['title'] = "LAPORAN KEPUASAN HARIAN TANGGAL ".tgl_indo(date('Y-m-d'));
		$data['kep_divisi']  = $this->Data_model->getdataharian();
			$this->load->view('v_lapdivisiexcel',$data);
			redirect('admin/lap_divisipdf','refresh');
		
		
		
	}
	public function lap_divisi_harianpdf(){
		$data['title'] = "LAPORAN KEPUASAN HARIAN TANGGAL ".tgl_indo(date('Y-m-d'));
		$data['kep_divisi']  = $this->Data_model->getdataharian();
		$this->load->view('v_lapdivisipdf',$data);
	}
	public function lap_divisi_periode(){
		if (isset($_POST['submit'])) {
			$data['url'] = "/feedback_button/admin/lap_divisi_periode";
			$data['divisi'] = $this->Data_model->getDivisi()->result();
			$data['tglmulai'] = date('Y-m-d',strtotime($_POST['tglmulai']));
			$data['tglselesai'] = date('Y-m-d',strtotime($_POST['tglselesai']));
			$data['title'] = "Laporan Divisi Periode ".tgl_indo($data['tglmulai'])." sd ".tgl_indo($data['tglselesai']);
			$data['kep_divisi']  = $this->Data_model->getdataperiode($data['tglmulai'] ,$data['tglselesai']);
			$this->template->load('template','v_lapdivisiperiode',$data);
		} else {
			$data['title'] = "Laporan per Divisi Per Periode";
		    $this->template->load('template','v_formperiode',$data);
		}
		
		
	}
	public function lap_divisi_periodepdf($tglmulai,$tglselesai){
		$data['title'] = "LAPORAN KEPUASAN DARI TANGGAL";
		$data['title'] = "Laporan Divisi Periode ".tgl_indo($tglmulai) .' sd '.tgl_indo($tglselesai);
		$data['kep_divisi']  = $this->Data_model->getdataperiode($tglmulai ,$tglselesai);
		$this->load->view('v_lapdivisipdf',$data);
	}
	public function lap_perdivisi($id=null){
		$data['lihat_divisi'] = $this->Data_model->getDivisi()->result();
		$data['divisi'] = $this->Data_model->getDivisiBy($id)->row();
		$this->template->load('template','v_perdivisi',$data);
	}
	 function getdata(){
        $data  = $this->Data_model->getdata();
        print_r(json_encode($data, true));
    }
    public function lap_user(){
		$data['user'] = $this->Data_model->getUser()->result();
		$data['kep_user']  = $this->Data_model->getdatauser();
		$this->template->load('template','v_lapuser',$data);
	}
	function getdatauser(){
        $data  = $this->Data_model->getdatauser();
        print_r(json_encode($data, true));
    }
    public function lap_peruser($id=null){
		$data['lihat_user'] = $this->Data_model->getUser()->result();
		$data['divisi'] = $this->Data_model->getUserBy($id)->row();
		$this->template->load('template','v_peruser',$data);
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
