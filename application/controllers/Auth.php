<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct()
    {
    	parent::__construct();
    	
    }
	function index(){
		chek_session_login();
		if(isset($_POST['go'])){
			$username = $_POST['username'];
			$password = $_POST['password'];
			$cek = $this->db->get_where('user',array('username'=>$username,'password'=>md5($password)));
			if($cek->num_rows()>0){
				if(isset($_POST['remember'])){
					set_cookie('cook_username',$username,time() + (10 * 365 * 24 * 60 * 60));
					set_cookie('cook_password',$password,time() + (10 * 365 * 24 * 60 * 60)); 
				}else{
					set_cookie('cook_username','',time() + (10 * 365 * 24 * 60 * 60));
			    	set_cookie('cook_password','',time() + (10 * 365 * 24 * 60 * 60)); 
				} 
				$user = $cek->row();
				$this->session->set_userdata(array('status_login'=>'oke','id_user'=>$user->id_user,'username'=>$username,'level'=>$user->level));
				
                if($user->level==1){
				  redirect('response_kepuasan');
			    }else{
			      redirect('admin');
			    }
			}else{
				$this->session->set_flashdata('error','Username/password salah');
				redirect('auth');
			}
		}else{
			$this->load->view('form_login'); 
		}
		
	}
	function logout(){
		
		$this->session->sess_destroy();
		redirect('auth');
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */