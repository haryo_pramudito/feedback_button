<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Response_kepuasan extends CI_Controller {

public function __construct()
{
	parent::__construct();
	$this->load->model('M_respon_kepuasan');
	hak_aksesuser();
    chek_session();
	
}
	public function index()
	{
		if(isset($_POST['submit'])){
           $data = array(
           			'id_user' => $this->session->userdata('id_user'),
           			'id_response'=> $_POST['submit'],
           			'datetime'   => date('Y-m-d H:i:s')
           	);
           $this->M_respon_kepuasan->simpan($data);
           $this->load->view('v_sukses');
		}else{
			$this->load->view('v_pilihan');
		}
	}

}

/* End of file Response_kepuasan.php */
/* Location: ./application/controllers/Response_kepuasan.php */