<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_user');
		hak_aksesadmin();
        chek_session();
	}
	public function index()
	{
		$data['user'] = $this->M_user->getData()->result();
		$this->template->load('template','v_indexuser',$data);
	}
    
    public function create()
	{ 
		if(isset($_POST['submit'])){
			$data = array(
				  'level' => $_POST['level'],
				  'id_divisi' => $_POST['id_divisi'],
				  'username' => $_POST['username'],
				  'password' => md5($_POST['password']),
				  );
			$this->M_user->create($data);
			redirect('user','refresh');
		}else{
			$data['divisi']  = $this->db->get('divisi')->result();
		    $this->template->load('template','v_createuser', $data);
		}
	}
	public function edit($id=null)
	{ 
		if(isset($_POST['submit'])){
			if($_POST['password']!=null){
			$data = array(
				  'level' => $_POST['level'],
				  'id_divisi' => $_POST['id_divisi'],
				  'username' => $_POST['username'],
				  'password' => md5($_POST['password']),
				  );
			}else{
				$data = array(
				  'level' => $_POST['level'],
				  'id_divisi' => $_POST['id_divisi'],
				  'username' => $_POST['username']
				  );
			}
			$this->M_user->edit($data,$id);
			redirect('user','refresh');
		}else{
			$data['divisi']  = $this->db->get('divisi')->result();
			$data['user']  = $this->M_user->getBy($id)->row();
		    $this->template->load('template','v_edituser', $data);
		}
	}
	public function delete($id=null){
		$this->db->delete('user', array('id_user'=>$id));
		redirect('user','refresh');
	}
}

/* End of file User.php */
/* Location: ./application/controllers/User.php */