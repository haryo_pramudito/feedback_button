<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

	function getData(){
		return $this->db->query("SELECT d.*,u.* FROM user u, divisi d WHERE u.id_divisi=d.id_divisi");
	}
	function getBy($id){
		return $this->db->get_where('user',array('id_user'=>$id));
	}
	function create($data){
		$this->db->insert('user',$data);
	}
    function edit($data,$id){
		$this->db->update('user',$data,array('id_user'=>$id));
	}
}

/* End of file M_user.php */
/* Location: ./application/models/M_user.php */