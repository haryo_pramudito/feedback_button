<?php
 
class Data_model extends CI_Model {
 
    /**
     * @desc load  db
     */
    function __construct() {
        parent::__Construct();
        $this->db = $this->load->database('default', TRUE, TRUE);
    }
 
    /**
     * @desc: Get data from company_performance table
     * @return: Array()
     */
    function getdata(){
        $this->db->select('*');
        $query = $this->db->query('SELECT * FROM v_kepuasan_div ORDER BY id_divisi ASC');
        return $query->result_array();
    }
    function getdataharian(){
        $query = $this->db->query("select `polling`.`divisi`.`id_divisi` AS `id_divisi`,`polling`.`divisi`.`nama_divisi` AS `divisi`,count(if((`polling`.`kepuasan`.`id_response` = 1),`polling`.`kepuasan`.`id_response`,NULL)) AS `sangat_puas`,count(if((`polling`.`kepuasan`.`id_response` = 2),`polling`.`kepuasan`.`id_response`,NULL)) AS `puas`,count(if((`polling`.`kepuasan`.`id_response` = 3),`polling`.`kepuasan`.`id_response`,NULL)) AS `cukup`,count(if((`polling`.`kepuasan`.`id_response` = 4),`polling`.`kepuasan`.`id_response`,NULL)) AS `tidak_puas` from (((`polling`.`user` join `polling`.`divisi` on((`polling`.`user`.`id_divisi` = `polling`.`divisi`.`id_divisi`))) join `polling`.`kepuasan` on((`polling`.`user`.`id_user` = `polling`.`kepuasan`.`id_user`))) join `polling`.`response` on((`polling`.`kepuasan`.`id_response` = `polling`.`response`.`id_response`))) where (cast(`polling`.`kepuasan`.`datetime` as date) = curdate()) group by `polling`.`user`.`id_divisi` order by `polling`.`divisi`.`id_divisi` ASC");
        return $query->result_array();
    }
    function getdataperiode($tglmulai,$tglselesai){
        $query = $this->db->query("select `polling`.`divisi`.`id_divisi` AS `id_divisi`,`polling`.`divisi`.`nama_divisi` AS `divisi`,count(if((`polling`.`kepuasan`.`id_response` = 1),`polling`.`kepuasan`.`id_response`,NULL)) AS `sangat_puas`,count(if((`polling`.`kepuasan`.`id_response` = 2),`polling`.`kepuasan`.`id_response`,NULL)) AS `puas`,count(if((`polling`.`kepuasan`.`id_response` = 3),`polling`.`kepuasan`.`id_response`,NULL)) AS `cukup`,count(if((`polling`.`kepuasan`.`id_response` = 4),`polling`.`kepuasan`.`id_response`,NULL)) AS `tidak_puas` from (((`polling`.`user` join `polling`.`divisi` on((`polling`.`user`.`id_divisi` = `polling`.`divisi`.`id_divisi`))) join `polling`.`kepuasan` on((`polling`.`user`.`id_user` = `polling`.`kepuasan`.`id_user`))) join `polling`.`response` on((`polling`.`kepuasan`.`id_response` = `polling`.`response`.`id_response`))) where cast(`polling`.`kepuasan`.`datetime` as date) BETWEEN '$tglmulai' and '$tglselesai' group by `polling`.`user`.`id_divisi` order by `polling`.`divisi`.`id_divisi` ASC");
        return $query->result_array();
    }
    function getDivisi(){
        return $this->db->query("SELECT * FROM user JOIN divisi ON user.id_divisi=divisi.id_divisi GROUP by user.id_divisi");
    }
    function getDivisiBy($id){
        return $this->db->query("SELECT divisi,
round( (sangat_puas / (sangat_puas+puas+cukup+tidak_puas) *100)) AS persen_sangatpuas,
( (puas / (sangat_puas+puas+cukup+tidak_puas)) *100) AS persen_puas,
( (cukup / (sangat_puas+puas+cukup+tidak_puas)) *100) AS persen_cukup,
( (tidak_puas / (sangat_puas+puas+cukup+tidak_puas)) *100) AS persen_tidakpuas FROM v_kepuasan_div WHERE id_divisi='$id' GROUP BY divisi, sangat_puas,puas,cukup,tidak_puas ");
    }


     function getUser(){
        return $this->db->query("SELECT * FROM user u join kepuasan k on k.id_user=u.id_user GROUP BY u.id_user");
    }
    function getdatauser(){
        $this->db->select('*');
        $query = $this->db->get('v_kepuasan_cs');
        return $query->result_array();
    }
    function getUserBy($id){
        return $this->db->query("SELECT 
nama_cs,
sangat_puas, 
puas,
cukup,
tidak_puas,
( (sangat_puas / (sangat_puas+puas+cukup+tidak_puas) *100)) AS persen_sangatpuas,
( (puas / (sangat_puas+puas+cukup+tidak_puas)) *100) AS persen_puas,
( (cukup / (sangat_puas+puas+cukup+tidak_puas)) *100) AS persen_cukup,
( (tidak_puas / (sangat_puas+puas+cukup+tidak_puas)) *100) AS persen_tidakpuas FROM v_kepuasan_cs WHERE id_cs='$id' GROUP BY divisi, sangat_puas,puas,cukup,tidak_puas");
    }
}