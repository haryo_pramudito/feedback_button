<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		
	}

	public function kta()
	{
		$this->load->library('cfpdf');
        $this->load->view('laporan/form_daftar');
	}
	public function rekomendasi()
	{
		$this->load->library('cfpdf');
        $this->load->view('laporan/form_rekomendasi');
	}
	public function p2kb()
	{
		$this->load->library('cfpdf');
        $this->load->view('laporan/form_p2kb');
	}
	public function registrasi_ulang()
	{
		$this->load->library('cfpdf');
        $this->load->view('laporan/form_reg_ulang');
	}
	public function etika_profesi()
	{
		$this->load->library('cfpdf');
        $this->load->view('laporan/form_etika_profesi');
	}
	public function kegiatanp2kb()
	{
		$this->load->library('cfpdf');
        $this->load->view('laporan/form_kegiatanp2kb');
	}
	public function suratketerangan()
	{
		$this->load->library('cfpdf');
        $this->load->view('laporan/surat_keterangan');
	}
	public function suratketerangansfm()
	{
		$this->load->library('cfpdf');
        $this->load->view('laporan/surat_keterangansfm');
	}
	public function daftarisi()
	{
		$this->load->library('cfpdf');
        $this->load->view('laporan/daftar_isi');
	}

}

/* End of file Laporan.php */
/* Location: ./application/controllers/Laporan.php */