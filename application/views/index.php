<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Treedesain.Com</title>
  <link rel="shortcut icon" href="<?php echo base_url();?>/images/logo.gif" type="image/x-icon" />

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url();?>css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?php echo base_url();?>css/plugins/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url();?>css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
 
</head>
 <style type="text/css">
 body {
   font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
   font-size: small;
   background: #fff;
 }
 #map {
   width: 100%;
   height: 700px;
   border: 1px solid #000;
 }
 .low_level {
background-color: #425F9C;
 
 color: #fff;
display: block;
 padding: 0.5em   ;
text-align: center;
font-size: 16px;
text-decoration: none;
 
 }


 </style>
<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" style="background-color: #425F9C;margin:0px" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="low_level" href="#" style="margin-top"><img style="height:30px;width:30px"  src="<?php echo base_url();?>images/logo.gif"> &nbsp;Treedesain.Com</a>
            </div>
            <!-- /.navbar-header -->

             <!-- /.navbar-top-links -->
 
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper" style="margin:0px">
        <h1 style="margin:0px"> Kepuasan Pelanggan </h1><hr style="margin:0px">
             <form  enctype="multipart/form-data" method="post" class="form1" id="frmsave" name="frmsave" method="post"  >
                <br><br>
                    <?php  if(!empty($query)) { foreach($query as $row) { ?> 
                         <input type="radio" name="radio" value="<?php echo $row->id;?>"> <label> <?php echo strtoupper($row->nama);?></label> &nbsp;&nbsp;
                    <?php }} ?>        
                     <br><br>
            <a onclick="return save()" class="btn btn-primary"><i class='fa fa-check'></i> SIMPAN</a> &nbsp; 
            <button   class="btn btn-danger"><i class='fa fa-times'></i> BATAL</button>
             </form> 
             <hr>
             <div id="div_chart" name="div_chart">
             </div>
        </div>
        <!-- /#page-wrapper -->
<footer>
<center>Artikel ini dibuat oleh <a href="http://www.treedesain.com">treedesain.com</a> </center>
</footer>
    </div>
    <!-- /#wrapper -->
      
    <script>
        function save(){
        $.ajax({
            url:'<?php echo base_url(); ?>home/simpan/',        
            type:'POST',
            data:$('#frmsave').serialize(),
            success:function(data){ 
                  $( "#div_chart" ).append(data);
                       loadchart1();    
             }
        });     
    }
    
    </script>
            <script>
    
    function loadchart1(){       
            $.ajax({
            url:'<?php echo base_url(); ?>home/hasil/',        
            type:'POST',
            data:$('#frmsave').serialize(),
            success:function(data){ 
                $( "#div_chart" ).html(data);
             }
        });     
   
         
    }
     
    
    </script>
   
    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/sb-admin-2.js"></script>
   <script>
    $(document).ready(function() {
          loadchart1();
    });
    </script>
</body>

</html>
