<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Index Kepuasan Pelanggan</title>
	<link rel="stylesheet" href="<?=base_url()?>emoticon/css/reset.css">
	
	<link rel="stylesheet" href="<?=base_url()?>dist/css/bootstrap.css"/>
	<link rel="stylesheet" href="<?=base_url()?>emoticon/css/main.css">
	<link rel="stylesheet" href="<?=base_url()?>emoticon/css/smoothness/jquery-ui-1.8.6.custom.css">
	<!-- iOS Related Items -->
	<meta name="viewport" content="width=920,maximum-scale=1,user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link rel="apple-touch-icon" href="<?=base_url()?>emoticon/images/apple-touch-icon.png">
	<!--[if lt IE 7]>
	<link rel="stylesheet" href="css/ie.css" type="text/css">
	<![endif]-->
	<script src="<?=base_url()?>emoticon/scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>emoticon/scripts/jquery-ui-1.8.6.custom.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>emoticon/scripts/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>emoticon/scripts/library.js" type="text/javascript"></script>
	<script src="<?=base_url()?>emoticon/scripts/factor.js" type="text/javascript"></script>
	<!--<script src="<?=base_url()?>dist/js/jquery-1.11.2.min.js"></script>-->
        <script src="<?=base_url()?>dist/js/bootstrap.js"></script>
	<script type="text/javascript">
		var BWS = window.BWS || {};
		// Game Configuration
		BWS.minimum_seed = 12;
		BWS.maximum_seed = 144;
		BWS.maximum_factor = 12;
		BWS.time_limit = 3; // in minutes
		BWS.seed_gestation = 1; // in seconds
		BWS.tree_growth_time = 1.5; // in seconds
		BWS.tree_display_time = 1; // in seconds
		// Initialize the Game
		$(document).ready(
			function() 
			{
				BWS.init();
			}
		);
		
	</script>
	<script type="text/javascript">document.webkitExitFullscreen();</script>
</head>
<body>
	<div id="container">

	<div id="stage">
	<div id="screen-loading">
		<h1>Loading... Please wait</h1>
		<noscript><p>Please note that JavaScript must be enabled in your browser for this program to function correctly.</p></noscript>
	</div>
	<div class="col-md-12">
	   <div class="col-md-10" style="margin-top: 50px"></div><div class="col-md-1"><a href="<?=base_url()?>auth/logout"><img src="<?=base_url()?>images/icon/logout.png"  height="80" width="80" title="kembali ke Beranda"/></a></div>
	   </div>
		<div id="screen-instructions" style="margin-bottom: 0px">
		<div id="instruction-content" style="margin-top: 0px">

			<font size="7"><b>Silahkan pilih tingkat kepuasan Anda!</b>	</font>
			<br>
			<br>
			<br>
			<p>
			<?=form_open('Response_kepuasan')?>
				<button class="tombol-sp" id="challenge-button" type="submit" name="submit" value="1" style="border: 0; background: transparent"><img src="<?=base_url()?>emoticon/images/sp.JPG" height="190" width="150" title="Sangat Puas" /></button>
				<button class="tombol-p" id="design-button"  type="submit" name="submit" value="2" style="border: 0; background: transparent"><img src="<?=base_url()?>emoticon/images/p.JPG" height="190"  width="150" title="Puas" /></button> 
				<button class="tombol-c" id="design-button"  type="submit" name="submit" value="3"  style="border: 0; background: transparent"><img src="<?=base_url()?>emoticon/images/c.JPG" height="190"  width="150"  title="Cukup" /></button>
				<button class="tombol-tp" id="design-button"  type="submit" name="submit" value="4"  style="border: 0; background: transparent"><img src="<?=base_url()?>emoticon/images/tp.JPG" height="190"  width="150" title="Tidak Puas" /></button>
			</form>
			</p>
		</div>
	</div>
	
	