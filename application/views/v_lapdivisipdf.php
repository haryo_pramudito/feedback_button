<?php
  if (isset($_POST['submit'])) {
  	  header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment;Filename=document_name.xls");
  }
?> 
 <!DOCTYPE html>
 <html>
 <head>
 	<title>LAPORAN KEPUASAN</title>
 	<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css" />
 </head>
 <style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>
 <style type="text/css">

		table {
			border-collapse: collapse;
		}
		caption {
			background: #D3D3D3;
		}
		th {
			background: #A7C942;
			border: 1px solid #98BF21;
			color: #ffffff;
			font-weight: bold;
			text-align: left;
		}
		td {
			border: 1px solid #98BF21;
			text-align: left;
			font-weight: normal;
			color: #000000;
		}
		tr:nth-child(odd) {
			background: #ffffff;
		}
		tbody tr:nth-child(odd) th {
			background: #ffffff;
			color: #000000;
		}
		tr:nth-child(even) {
			background: #EAF2D3;
		}
		tbody tr:nth-child(even) th {
			background: #EAF2D3;
			color: #000000;
		}
		#target {
			width: 600px;
			height: 400px;
		}
	</style>
<body >

												   <div id="lap_divisi">

											


<script src="<?=base_url()?>charts/highcharts.js"></script>
<script src="<?=base_url()?>charts/data.js"></script>
<script src="<?=base_url()?>charts/exporting.js"></script>
<br><br>

<center>
<div class="col-md-12">
<table width="100%" border="0">
<tbody>
<tr style="height: 69px;">
<td style="height: 69px; width: 133px;" ><img src="<?=base_url()?>assets/images/avatars/logobpn.png" alt="" width="63%" /></td>
<td style="height: 69px; width: 618px;" >
<h2><center><strong>BADAN PERTANAHAN NASIONAL REPUBLIK INDONESIA</strong></h2><center>
KANTOR PERTANAHAN KOTA BOGOR<br />PROVINSI JAWA BARAT<strong><strong><br /></strong></strong>Jl. Ahmad Yani Blok Texan No.7, Tanah Sareal, Tanah Sereal, Kota Bogor, Jawa Barat 16161<strong><strong><br /></strong></strong></td>
</tr>
</tbody>
</table>

<br><br><br>
</div>
<h3><?=$title?></h3></center>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<div class="col-md-12">

<table id="datatable" border="1">
    <thead>
        <tr>
				<th>Nama Divisi</th>
				<th>Sangat Puas</th>
				<th>Puas</th>
				<th>Cukup</th>	
				<th>Tidak Puas</th>	
			</tr>
    </thead>
    <tbody>
        <?php foreach($kep_divisi as $kep):?>
			<tr>
				<th><?=$kep['divisi']?></th>
				<td><?=$kep['sangat_puas']?></td>
				<td><?=$kep['puas']?></td>
				<td><?=$kep['cukup']?></td>
				<td><?=$kep['tidak_puas']?></td>
			</tr>
		 <?php endforeach?>
    </tbody>
</table>


</div>
<div class="col-md-12">
	<div class="col-md-9"></div>
	<div class="col-md-3"><input id="printpagebutton" type="button" value="PRINT PDF" class="btn btn-primary btn-sm" onclick="printpage()"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   <?php
      $tglmulai = $this->uri->segment(3);
      $tglselesai = $this->uri->segment(4);
      $url = $tglmulai!=null?"$tglmulai/$tglselesai":'';
   ?>

    <?php
     if ($this->uri->segment(2)=='lap_divisi_harianpdf') {
     	echo anchor('admin/lap_divisiexcelharian/','EXPORT EXCEL',['class'=>"btn btn-primary btn-sm" ,'id'=>"excelpagebutton"]);
     } else {
     	echo anchor('admin/lap_divisiexcel/'.$url,'EXPORT EXCEL',['class'=>"btn btn-primary btn-sm" ,'id'=>"excelpagebutton"]);
     }
     
      
      ?>
	</div>
</div>
<script type="text/javascript">
	Highcharts.chart('container', {
    data: {
        table: 'datatable'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Jumlah'
        },
         stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
            }
        }
    },

    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    },
    plotOptions: {
            series: {
                pointWidth: 15
            }
        }
    
});

</script>
</body>


	</div>
<script type="text/javascript">
	function printpage() {
        //Get the print button and put it into a variable
        var printButton = document.getElementById("printpagebutton");
        var excelButton = document.getElementById("excelpagebutton");
        //Set the print button visibility to 'hidden' 
        printButton.style.visibility = 'hidden';
        excelButton.style.visibility = 'hidden';
        //Print the page content
        window.print()
        printButton.style.visibility = 'visible';
        excelButton.style.visibility = 'visible';
    }
</script>
												
												
				 
				