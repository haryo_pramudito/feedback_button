 <script type="text/javascript">
     
	function timedRedirect() {
    setTimeout("location.href = '/feedback_button/admin/lap_user'",10000);
}
  </script>
  <body onload="timedRedirect()">
<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
						<strong><?=tgl_indo(date('Y-m-d')).' pukul '?><span id="clock"></span></strong>
						</div><!-- /.nav-search -->

						
					</div>
					<div class="page-content">

						<div class="page-header">
							<h1>
								Laporan
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Laporan per Customer Service
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								

								<div class="row">
									<div class="space-6"></div>

									

									<div class="vspace-12-sm"></div>

									<div class="col-sm-12">
										<div class="widget-box">
											<div class="widget-header widget-header-flat widget-header-small">
												<h5 class="widget-title">
													<i class="ace-icon fa fa-signal"></i>
													Laporan per Customer Service
												</h5>

												<div class="widget-toolbar no-border">
													<div class="inline dropdown-hover">
														<button class="btn btn-minier btn-primary">
															Pilih Customer Service
															<i class="ace-icon fa fa-angle-down icon-on-right bigger-110"></i>
														</button>

														<ul class="dropdown-menu dropdown-menu-right dropdown-125 dropdown-lighter dropdown-close dropdown-caret">
														<?php foreach($user as $u):?>
															<li class="active">
																<a href="<?=base_url()?>admin/lap_peruser/<?=$u->id_user?>" class="blue" >
																	<i class="ace-icon fa fa-caret-right bigger-110">&nbsp;</i>
																	<?=$u->username?>
																</a>
															</li>
														<?php endforeach?>
														</ul>
													</div>
												</div>
												   <div id="lap_divisi">

											</div>

											<div class="widget-body">
												<div class="widget-main">
												    	<script src="<?=base_url()?>charts/highcharts.js"></script>
<script src="<?=base_url()?>charts/data.js"></script>
<script src="<?=base_url()?>charts/exporting.js"></script>
<br><br>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<table id="datatable" hidden="">
    <thead>
        <tr>
				<th></th>
				<th>Sangat Puas</th>
				<th>Puas</th>
				<th>Cukup</th>	
				<th>Tidak Puas</th>	
			</tr>
    </thead>
    <tbody>
        <?php foreach($kep_user as $kep):?>
			<tr>
				<th><?=$kep['nama_cs']?></th>
				<td><?=$kep['sangat_puas']?></td>
				<td><?=$kep['puas']?></td>
				<td><?=$kep['cukup']?></td>
				<td><?=$kep['tidak_puas']?></td>
			</tr>
		 <?php endforeach?>
    </tbody>
</table>
<script type="text/javascript">
	Highcharts.chart('container', {
    data: {
        table: 'datatable'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Jumlah'
        }
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    },
});
</script>
												   </div>

												
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div><!-- /.col -->
								</div><!-- /.row -->
								

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
				<script src="<?=base_url()?>assets/js/jquery-2.1.4.min.js"></script>
				<script type="text/javascript">
						function divisi(url){
							$.ajax({
								url : "lap_perdivisi/"+url,
								success : function(html){
									$('#lap_divisi').html(html);
								}
							});
						}
				</script>
</body>