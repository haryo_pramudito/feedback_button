<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
						<strong><?=tgl_indo(date('Y-m-d')).' pukul '?><span id="clock"></span></strong>
						</div><!-- /.nav-search -->

						
					</div>

					<div class="page-content">

						<div class="page-header">
							<h1>
								User
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Data User
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								

								<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="row">
									<div class="col-xs-12">
								<div class="col-md-10"></div>	<div class="col-md-2"><?=anchor('user/create','tambah data',['class'=>'btn btn-primary btn-sm'])?></div><br /><br />
										<table id="simple-table" class="table  table-bordered table-hover">
											<thead>
											
												<tr>
													<th class="detail-col">No</th>
													<th>Username</th>
													<th>Divisi</th>
													<th class="hidden-480">Hak akses</th>

													<th>Aksi</th>
												</tr>
                                           
											</thead>

											<tbody>
												<?php $no= 1; foreach($user as $u):?>
												<tr>

													<td class="center">
														<?=$no?>
													</td>

													<td>
														<?=$u->username?>
													</td>
													<td><?=$u->nama_divisi?></td>
													<td class="hidden-480"><?=$u->level==0?'Administrator':'Customer Service'
													?></td>


													<td>

															<a href="<?=base_url()?>user/edit/<?=$u->id_user?>" class="btn btn-xs btn-info">
																<i class="ace-icon fa fa-pencil bigger-120"></i>
															</a>

															<a onclick="return confirm('yakin!')" href="<?=base_url()?>user/delete/<?=$u->id_user?>" class="btn btn-xs btn-danger">
																<i class="ace-icon fa fa-trash-o bigger-120"></i>
															</a>
														</div>

														
														</div>
													</td>
												</tr>
  												<?php $no++; endforeach?>
												

												
											</tbody>
										</table>
									</div><!-- /.span -->
								</div><!-- /.row -->

								<div class="hr hr32 hr-dotted"></div>
								

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>