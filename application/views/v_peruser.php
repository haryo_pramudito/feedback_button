<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							
						</ul><!-- /.breadcrumb -->
						
					</div>

					<div class="page-content">

						<div class="page-header">
							<h1>
								Laporan
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									laporan per Customer Service
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								

								<div class="row">
									<div class="space-6"></div>

									

									<div class="vspace-12-sm"></div>

									<div class="col-sm-12">
										<div class="widget-box">
											<div class="widget-header widget-header-flat widget-header-small">
												<h5 class="widget-title">
													<i class="ace-icon fa fa-signal"></i>
													<?=$divisi->nama_cs?>
												</h5>

												<div class="widget-toolbar no-border">
													<div class="inline dropdown-hover">
														<button class="btn btn-minier btn-primary">
															Pilih Customer Service
															<i class="ace-icon fa fa-angle-down icon-on-right bigger-110"></i>
														</button>

														<ul class="dropdown-menu dropdown-menu-right dropdown-125 dropdown-lighter dropdown-close dropdown-caret">
															<?php foreach($lihat_user as $u):?>
															<li class="active">
																<a href="<?=base_url()?>admin/lap_peruser/<?=$u->id_user?>" class="blue" >
																	<i class="ace-icon fa fa-caret-right bigger-110">&nbsp;</i>
																	<?=$u->username?>
																</a>
															</li>
														<?php endforeach?>
														</ul>
													</div>
												</div>

											</div>

											<div class="widget-body">
												<div class="widget-main">
												   <br><br><br><br>
													<div id="piechart-placeholder"></div>
													<br><br><br><br>
													<div class="hr hr8 hr-double"></div>

													<a href="javascript:history.back()" class="btn btn-grey">
														<i class="ace-icon fa fa-arrow-left"></i>
														Kembali
													</a>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div><!-- /.col -->
								</div><!-- /.row -->

								<div class="hr hr32 hr-dotted"></div>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
				<script src="<?=base_url()?>assets/js/jquery-2.1.4.min.js"></script>
				
