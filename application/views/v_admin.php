 <script type="text/javascript">
      // Load the Visualization API and the line package.
      google.charts.load('current', {'packages':['bar']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);
  
    function drawChart() {
  
        $.ajax({
        type: 'POST',
        url: "<?=base_url()?>admin/getdata",
          
        success: function (data1) {
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable();
  
      data.addColumn('string', 'Divisi');
      data.addColumn('number', 'Sangat Puas');
      data.addColumn('number', 'Puas');
      data.addColumn('number', 'Cukup');
      data.addColumn('number', 'Tidak Puas');
      var jsonData = $.parseJSON(data1);
      
      for (var i = 0; i < jsonData.length; i++) {
            data.addRow([jsonData[i].divisi, parseInt(jsonData[i].sangat_puas), parseInt(jsonData[i].puas),parseInt(jsonData[i].cukup), parseInt(jsonData[i].tidak_puas)]);
      }
      var options = {
        chart: {
          title: '',
          subtitle: 'Menampilkan tingkat kepuasan per divisi'
        },
        width: 500,
        height: 250,
        axes: {
          x: {
            0: {side: 'bottom'}
          }
        }
         
      };
      var chart = new google.charts.Bar(document.getElementById('bar_chart'));
      chart.draw(data, options);
       }
     });
    }
  </script>
   <script type="text/javascript">
      // Load the Visualization API and the line package.
      google.charts.load('current', {'packages':['bar']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChartb);
  
    function drawChartb() {
  
        $.ajax({
        type: 'POST',
        url: "<?=base_url()?>admin/getdatauser",
          
        success: function (data1) {
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable();
  
      data.addColumn('string', 'Nama Customer');
      data.addColumn('number', 'Sangat Puas');
      data.addColumn('number', 'Puas');
      data.addColumn('number', 'Cukup');
      data.addColumn('number', 'Tidak Puas');
      var jsonData = $.parseJSON(data1);
      
      for (var i = 0; i < jsonData.length; i++) {
            data.addRow([jsonData[i].nama_cs, parseInt(jsonData[i].sangat_puas), parseInt(jsonData[i].puas),parseInt(jsonData[i].cukup), parseInt(jsonData[i].tidak_puas)]);
      }
      var options = {
        chart: {
          title: '',
          subtitle: 'Menampilkan tingkat kepuasan per user'
        },
        width: 500,
        height: 250,
        axes: {
          x: {
            0: {side: 'bottom'}
          }
        }
         
      };
      var chart = new google.charts.Bar(document.getElementById('bar_chartb'));
      chart.draw(data, options);
       }
     });
    }
  </script>
<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
              
            </ul><!-- /.breadcrumb -->

            <div class="nav-search" id="nav-search">
            <strong><?=tgl_indo(date('Y-m-d')).' pukul '?><span id="clock"></span></strong>
            </div><!-- /.nav-search -->

            
          </div>

					<div class="page-content">

						<div class="page-header">
							<h1>
							   Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-md-3">
								<!-- PAGE CONTENT BEGINS -->
								

                                <img class="nav-user-photo" src="<?=base_url()?>assets/images/avatars/logobpn.png" alt=" "/>

								

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
							<div class="col-md-9">
								<!-- PAGE CONTENT BEGINS -->
								<div class="alert alert-block alert-success">
									<button type="button" class="close" data-dismiss="alert">
										<i class="ace-icon fa fa-times"></i>
									</button>

									<i class="ace-icon fa fa-check green"></i>

									Selamat Datang
									<strong class="brown">
										<?=$this->session->userdata('username');?>
										
									</strong>
									Di Aplikasi Index Kepuasan Customer<strong class="brown"> BPN Kota Bogor</strong>, gunakan menu navigasi yang ada di sebelah kiri untuk memanajemen sistem ini.<br><br><br><br><br>
								</div>
								
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
				<script src="<?=base_url()?>assets/js/jquery-2.1.4.min.js"></script>
				<script type="text/javascript">
						function divisi(url){
							$.ajax({
								url : "lap_perdivisi/"+url,
								success : function(html){
									$('#lap_divisi').html(html);
								}
							});
						}
				</script>
