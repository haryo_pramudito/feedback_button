 <script type="text/javascript">
    
	function timedRedirect() {
     setTimeout(function(){
     	window.location.reload(true)
     },10000);
}
  </script>
 <style type="text/css">
		table {
			border-collapse: collapse;
		}
		caption {
			background: #D3D3D3;
		}
		th {
			background: #A7C942;
			border: 1px solid #98BF21;
			color: #ffffff;
			font-weight: bold;
			text-align: left;
		}
		td {
			border: 1px solid #98BF21;
			text-align: left;
			font-weight: normal;
			color: #000000;
		}
		tr:nth-child(odd) {
			background: #ffffff;
		}
		tbody tr:nth-child(odd) th {
			background: #ffffff;
			color: #000000;
		}
		tr:nth-child(even) {
			background: #EAF2D3;
		}
		tbody tr:nth-child(even) th {
			background: #EAF2D3;
			color: #000000;
		}
		#target {
			width: 600px;
			height: 400px;
		}
	</style>
<body onload="timedRedirect()">
<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
						<strong><?=tgl_indo(date('Y-m-d')).' pukul '?><span id="clock"></span></strong>
						</div><!-- /.nav-search -->

						
					</div>

					<div class="page-content">

						<div class="page-header">
							<h1>
								Laporan
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Laporan per divisi
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								

								<div class="row">
									<div class="space-6"></div>

									

									<div class="vspace-12-sm"></div>

									<div class="col-sm-12">
										<div class="widget-box">
											<div class="widget-header widget-header-flat widget-header-small">
												<h5 class="widget-title">
													<i class="ace-icon fa fa-signal"></i>
													<?=$title?>
												</h5>

												<div class="widget-toolbar no-border">
													<div class="inline dropdown-hover">
													
														<a href="<?=base_url().'admin/lap_divisi_periodepdf/'.$tglmulai.'/'.$tglselesai?>" target="_blank" class="btn btn-app btn-inverse btn-xs">

												<i class="ace-icon fa fa-share "></i>
												EXPORT
											</a>

													
													</div>
												</div>
												   <div id="lap_divisi">

											</div>

											<div class="widget-body">
												<div class="widget-main">


	<script src="<?=base_url()?>charts/highcharts.js"></script>
<script src="<?=base_url()?>charts/data.js"></script>
<script src="<?=base_url()?>charts/exporting.js"></script>
<br><br>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<table id="datatable" hidden="">
    <thead>
        <tr>
				<th></th>
				<th>Sangat Puas</th>
				<th>Puas</th>
				<th>Cukup</th>	
				<th>Tidak Puas</th>	
			</tr>
    </thead>
    <tbody>
        <?php foreach($kep_divisi as $kep):?>
			<tr>
				<th><?=$kep['divisi']?></th>
				<td><?=$kep['sangat_puas']?></td>
				<td><?=$kep['puas']?></td>
				<td><?=$kep['cukup']?></td>
				<td><?=$kep['tidak_puas']?></td>
			</tr>
		 <?php endforeach?>
    </tbody>
</table>
<script type="text/javascript">
	Highcharts.chart('container', {
    data: {
        table: 'datatable'
    },
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    yAxis: {
        allowDecimals: false,
        title: {
            text: 'Jumlah'
        },
    },
    tooltip: {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                this.point.y + ' ' + this.point.name.toLowerCase();
        }
    },
});
</script>
	
	</div>
   <div id="target2">
												   </div>

												
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div><!-- /.col -->
								</div><!-- /.row -->
								

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
				 
				</body>
