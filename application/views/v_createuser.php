<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
						<strong><?=tgl_indo(date('Y-m-d')).' pukul '?><span id="clock"></span></strong>
						</div><!-- /.nav-search -->

						
					</div>

					<div class="page-content">

						<div class="page-header">
							<h1>
								User
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Form Tambah Data User
								</small>
							</h1>
						</div><!-- /.page-header -->
					<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<form class="form-horizontal" role="form" method="POST" action="<?=base_url()?>user/create">
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Username</label>

										<div class="col-sm-9">
											<input type="text" required="" id="form-field-1" name="username" placeholder="Username" class="col-xs-10 col-sm-5" />
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-3 required control-label no-padding-right" for="form-field-1"> Password</label>

										<div class="col-sm-9">
											<input type="text" id="form-field-1" required="" name="password" placeholder="Password" class="col-xs-10 col-sm-5" />
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Divisi</label>

										<div class="col-sm-9">
											<select name="id_divisi" required="" class="col-xs-10 col-sm-5">
											<option value="">==PILIH DIVISI==</option>
											 <?php foreach($divisi as $d):?>
												<option value="<?=$d->id_divisi?>"><?=$d->nama_divisi?></option>
											 <?php endforeach?>
											</select>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Level</label>

										<div class="col-sm-9">
											<div class="radio">
													<label>
														<input name="level" required="" checked="" value="0" type="radio" class="ace" />
														<span class="lbl"> Admin</span>
													</label>
												</div>
												<div class="radio">
													<label>
														<input name="level" required="" value="1" type="radio" class="ace" />
														<span class="lbl"> Customer Service</span>
													</label>
												</div>


										</div>
									</div>
									

									<div class="clearfix form-actions">

										<div class="col-md-2">
											<a href="javascript:history.back()" class="btn btn-grey">
												<i class="ace-icon fa fa-arrow-left"></i>
												Kembali
											</a>
										</div>
										<div class="col-md-10">
											<button class="btn btn-info" type="submit" name="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button>
										</div>
									</div>

									<div class="hr hr-24"></div>






								</form>

								

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
				<script src="<?=base_url()?>assets/js/jquery-2.1.4.min.js"></script>
				<script type="text/javascript">
						function divisi(url){
							$.ajax({
								url : "lap_perdivisi/"+url,
								success : function(html){
									$('#lap_divisi').html(html);
								}
							});
						}
				</script>