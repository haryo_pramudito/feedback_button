<html>
<head>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
    <script type="text/javascript">
      // Load the Visualization API and the line package.
      google.charts.load('current', {'packages':['bar']});
      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);
  
    function drawChart() {
  
        $.ajax({
        type: 'POST',
        url: "<?=base_url()?>charts/getdata",
          
        success: function (data1) {
        // Create our data table out of JSON data loaded from server.
        var data = new google.visualization.DataTable();
  
      data.addColumn('string', 'Divisi');
      data.addColumn('number', 'Sangat Puas');
      data.addColumn('number', 'Puas');
      data.addColumn('number', 'Cukup');
      data.addColumn('number', 'Tidak Puas');
      var jsonData = $.parseJSON(data1);
      
      for (var i = 0; i < jsonData.length; i++) {
            data.addRow([jsonData[i].divisi, parseInt(jsonData[i].sangat_puas), parseInt(jsonData[i].puas),parseInt(jsonData[i].cukup), parseInt(jsonData[i].tidak_puas)]);
      }
      var options = {
        chart: {
          title: 'Company Performance',
          subtitle: 'Show Sales and Expense of Company'
        },
        width: 1024,
        height: 800,
        axes: {
          x: {
            0: {side: 'bottom'}
          }
        }
         
      };
      var chart = new google.charts.Bar(document.getElementById('bar_chart'));
      chart.draw(data, options);
       }
     });
    }
  </script>
</head>
<body>
  
  <div id="bar_chart"></div>
</body>
</html>