<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Index Kepuasan Pelanggan</title>
	<link rel="stylesheet" href="<?=base_url()?>emoticon/css/reset.css">
	
	<link rel="stylesheet" href="<?=base_url()?>dist/css/bootstrap.css"/>
	<link rel="stylesheet" href="<?=base_url()?>emoticon/css/main.css">
	<link rel="stylesheet" href="<?=base_url()?>emoticon/css/smoothness/jquery-ui-1.8.6.custom.css">
	<!-- iOS Related Items -->
	<meta name="viewport" content="width=920,maximum-scale=1,user-scalable=0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link rel="apple-touch-icon" href="<?=base_url()?>emoticon/images/apple-touch-icon.png">
	<!--[if lt IE 7]>
	<link rel="stylesheet" href="css/ie.css" type="text/css">
	<![endif]-->
	<script src="<?=base_url()?>emoticon/scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>emoticon/scripts/jquery-ui-1.8.6.custom.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>emoticon/scripts/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
	<script src="<?=base_url()?>emoticon/scripts/library.js" type="text/javascript"></script>
	<script src="<?=base_url()?>emoticon/scripts/factor.js" type="text/javascript"></script>
	<script src="<?=base_url()?>dist/js/jquery-1.11.2.min.js"></script>
        <script src="<?=base_url()?>dist/js/bootstrap.js"></script>
	<script type="text/javascript">
		var BWS = window.BWS || {};
		// Game Configuration
		BWS.minimum_seed = 12;
		BWS.maximum_seed = 144;
		BWS.maximum_factor = 12;
		BWS.time_limit = 3; // in minutes
		BWS.seed_gestation = 1; // in seconds
		BWS.tree_growth_time = 1.5; // in seconds
		BWS.tree_display_time = 1; // in seconds
		// Initialize the Game
		$(document).ready(
			function() 
			{
				BWS.init();
			}
		);
		
	</script>
</head>
<body onload="timedRedirect()">
	<div id="container">
	<div id="stage">
	
	
		<div id="instruction-content">

	<div id="screen-instructions">
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>

		<div class="alert alert-success"><h1><b>Terima Kasih Telah memberikan respon kepuasan</b></h1></div>
		</div>
	</div>

<script type="text/JavaScript">

function timedRedirect() {
    setTimeout("location.href = '/feedback_button/Response_kepuasan'",2000);
}
</script>