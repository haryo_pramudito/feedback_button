
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=1,initial-scale=1,user-scalable=1" />
	<title>KEPUASAN PELANGGAN</title>
	<link rel="shortcut icon" href="images/favicon.jpg" type="image/x-icon"/>
	<link href="http://fonts.googleapis.com/css?family=Lato:100italic,100,300italic,300,400italic,400,700italic,700,900italic,900" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/assets/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>public/assets/bootstrap/css/styles.css" />
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<section class="container">
			<section class="login-form">
				<form method="post" action="<?=base_url()?>Auth" role="login">
					<img src="<?=base_url()?>public/assets/images/logo.png" class="img-responsive" alt="" />
					<?php if($this->session->flashdata('error')):?>
				    <div class="alert alert-danger"><?=$this->session->flashdata('error')?></div>
				    <?php endif?>
					<input type="username" name="username" value="<?=get_cookie('cook_username')?>" placeholder="Username" required class="form-control input-lg" />
					<input type="password" name="password" value="<?=get_cookie('cook_password')?>" placeholder="Password" required  class="form-control input-lg" />
					
					<input type="checkbox" checked name="remember" value="1" /> Ingatkan Saya<br />
					
					
					<button type="submit" name="go" class="btn btn-lg btn-block btn-primary">Sign in</button>
				</form>
				</section>
	</section>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="<?=base_url()?>public/assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript"></script></body>
</html>