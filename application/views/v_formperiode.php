<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
						<strong><?=tgl_indo(date('Y-m-d')).' pukul '?><span id="clock"></span></strong>
						</div><!-- /.nav-search -->

						
					</div>

					<div class="page-content">

						<div class="page-header">
							<h1>
								Laporan
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Laporan Divisi Per Periode
								</small>
							</h1>
						</div><!-- /.page-header -->
					<div class="row">
							<div class="col-xs-12">
	  <script type="text/javascript" src="<?=base_url()?>datepicker/jquery-2.0.2.js"></script>
      <script type="text/javascript" src="<?=base_url()?>datepicker/jquery-ui.js"></script>
      <link rel="stylesheet" type="text/css" href="<?=base_url()?>datepicker/jquery-ui.css">
      <link rel="stylesheet" type="text/css" href="<?=base_url()?>datepicker/datepicker3.css">
    
  
      <script type="text/javascript" src="<?=base_url()?>datepicker/bootstrap-datepicker.js"></script>
   
			 <script type='text/javascript'>//<![CDATA[
$(window).load(function(){
    $('.datepickstart').datepicker({
     autoclose: true,
     todayHighlight: true,
     format: 'dd-mm-yyyy',
      endDate : new Date()
    });
    $('.datepickend').datepicker({
     autoclose: true,
     todayHighlight: true,
     format: 'dd-mm-yyyy',
      endDate : new Date()
    });
    
});//]]> 

</script>
            <?=form_open('admin/lap_divisi_periode')?>
				<div class="row">
				    <div class="form-group" >
				        <div class="col-md-3">
				                <div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
				                    <input type="text" type="text" name="tglmulai" required class="form-control datepickstart" placeholder="Tanggal Mulai" />
				                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				            </div>
				        </div>
				        <div class="col-md-3">
				                <div class="input-group date " data-date="" data-date-format="yyyy-mm-dd">
				                     <input type="text" type="text" name="tglselesai"  required="" class="form-control datepickend" placeholder="Tanggal Selesai" />
				                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				            </div>
				        </div>
				         <div class="col-md-1">
				                   <button type="submit" name="submit" class="btn btn-primary btn-sm">Lihat</button>
				            </div>
				        </div>
				    </div>
		    <?=form_close()?>
  		   
										<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>