<?php
/** PHPExcel */
 

// Create new PHPExcel object
$object = new PHPExcel();
 
// Set properties
$object->getProperties()->setCreator("Tempo")
               ->setLastModifiedBy("Tempo")
               ->setCategory("Approve by ");
// Add some data

//$object->getActiveSheet()->getColumnDimensionByColumn('A')->setAutoSize(false);
//$object->getActiveSheet()->getColumnDimensionByColumn('A')->setWidth('50');
$object->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
$object->getActiveSheet()->getColumnDimension('A')->setWidth('50');
$object->getActiveSheet()->getColumnDimension('B')->setAutoSize ( true );
$object->getActiveSheet()->getColumnDimension('C')->setAutoSize ( true );
$object->getActiveSheet()->getColumnDimension('D')->setAutoSize ( true );
$object->getActiveSheet()->getColumnDimension('E')->setAutoSize ( true );
$object->getActiveSheet()->mergeCells('A1:F1');
$object->getActiveSheet()->mergeCells('A2:F2');
$object->getActiveSheet()->mergeCells('A3:F3');
$object->getActiveSheet()->mergeCells('A4:F4');
$object->getActiveSheet()->mergeCells('A5:F5');
$object->getActiveSheet()->mergeCells('A6:F6');
$object->getActiveSheet()->mergeCells('A7:F7');
$object->setActiveSheetIndex(0)
            ->setCellValue('A1', 'BADAN PERTANAHAN NASIONAL REPUBLIK INDONESIA')
            ->setCellValue('A2', 'KANTOR PERTANAHAN KOTA BOGOR')
            ->setCellValue('A3', 'PROVINSI JAWA BARAT')
            ->setCellValue('A4', 'Jl. Ahmad Yani Blok Texan No.7, Tanah Sareal, Tanah Sereal, Kota Bogor, Jawa Barat 16161')
            ->setCellValue('A6', $title)
            ->setCellValue('A8', 'Nama Divisi')
            ->setCellValue('B8', 'Sangat Puas')
            ->setCellValue('C8', 'Puas')
            ->setCellValue('D8', 'Cukup')
            ->setCellValue('E8', 'Tidak Puas');
$object->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$object->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true)->setSize(16);
$object->getActiveSheet()->getStyle('A2:F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$object->getActiveSheet()->getStyle('A3:F3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$object->getActiveSheet()->getStyle('A4:F4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$object->getActiveSheet()->getStyle('A6:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$object->getActiveSheet()->getStyle("A8:B8")->getFont()->setBold(true);
$object->getActiveSheet()->getStyle('A8:B8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF808080');
$object->getActiveSheet()->getStyle('C8:D8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF808080');
$object->getActiveSheet()->getStyle('E8')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF808080');
$object->getActiveSheet()->getStyle("C8:D8")->getFont()->setBold(true);
$object->getActiveSheet()->getStyle("E8")->getFont()->setBold(true);
//add data
$counter=9;
$ex = $object->setActiveSheetIndex(0);
foreach($kep_divisi as $kep){
           
        
          $ex->setCellValue("A".$counter,$kep['divisi']);
          $ex->setCellValue("B".$counter,$kep['sangat_puas']);
          $ex->setCellValue("C".$counter,$kep['puas']);
          $ex->setCellValue("D".$counter,$kep['cukup']);
          $ex->setCellValue("E".$counter,$kep['tidak_puas']);
          $counter=$counter+1;
           
}   
 
// Rename sheet
//$object->getActiveSheet()->setTitle('gfhjgjhj');
 
 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$object->setActiveSheetIndex(0);
 
 $objDrawing = new PHPExcel_Worksheet_Drawing();
$objDrawing->setName('Logo');
$objDrawing->setDescription('Logo');
$objDrawing->setPath('assets/images/avatars/logobpn.png');
$objDrawing->setCoordinates('A1');
$objDrawing->setHeight(100);
$objDrawing->setWidth(100);
$objDrawing->setWorksheet($object->getActiveSheet());
$url = $this->uri->segment(2);
$filename = '';
if ($url=='lap_divisiexcelharian') {
    $filename = $url.'_'.tgl_indo(date('Y-m-d')).'.xlsx';
}else{
    $tglmulai = $this->uri->segment(3);
    $tglselesai = $this->uri->segment(4);
    $filename = $tglmulai==null?'lap_semua_periode.xlsx':'lap_periode_'.tgl_indo($tglmulai).' sd '.tgl_indo($tglselesai).'.xlsx';
}

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');
 
$objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel2007');
$objWriter->save('php://output');
exit;
?>