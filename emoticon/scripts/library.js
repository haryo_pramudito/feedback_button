var BWS = window.BWS || {};

BWS.array_search = function(needle, haystack) {
	if (haystack.length == 0) { return false; }
	for (var i = 0; i < haystack.length; i++) {
		if (needle == haystack[i]) { return true; }
	}
	return false;
}

BWS.array_property_search = function(needle, haystack, prop) {
	if (haystack.length == 0) { return false; }
	for (var i = 0; i < haystack.length; i++) {
		if (needle == haystack[i][prop]) { return true; }
	}
	return false;
}

BWS.random_number = function(min, max) {
	
	// this function returns a random number in the range from min to max
	var range = Math.abs(max - min) + 1;
	var number = Math.floor(range*Math.random());
	if (number == range) { number = number - 1; }
	return number + Math.min(max,min);
	
}

BWS.random_element = function(this_array) {

	// this function returns a random element from an array
	var n = BWS.random_number(0, (this_array.length - 1));
	return this_array[n];

}

BWS.remove_element = function(this_index, this_array) {

	// this function removes the selected index from the array and re-numbers
	var new_array = [];
	
	for (var i = 0; i < this_array.length; i++) {
		if (i !=  this_index) {
			new_array.push(this_array[i]);
		}
	}
	
	return new_array;

}

BWS.preload_images = function(images) {
	for (i = 0; i < images.length; i++) {
		game_images[i] = new Image();
		game_images[i].src=images[i];
	}
}

BWS.uc_first = function(str) {

	return str.charAt(0).toUpperCase() + str.slice(1);

}

BWS.format_time = function(time) {
	
	var display_time = '';
	
	var minutes = Math.floor(time/60);
	var seconds = time % 60;

	if (minutes.toString().length == 1) { minutes = '0' + minutes; }
	if (seconds.toString().length == 1) { seconds = '0' + seconds; }

	display_time = minutes + ':' + seconds;
	
	return display_time;

}


BWS.update_game_timer = function() {

	var display_time = BWS.format_time(BWS.game_time);

	$('#timer').html(display_time);

	BWS.game_time -= 1;

	if (BWS.game_time < 0) {
	
		BWS.end_game();
		
	} else {
	
		BWS.game_timeout = setTimeout("BWS.update_game_timer();", 1000);
	
	}
	
} 

