var BWS = window.BWS || {};

BWS.init = function() {

	BWS.game_timer = 0;
	BWS.click_tracker = [];
	BWS.seeds = [];
	BWS.game_over = 1;

	// The trees that a user creates
	BWS.trees = [];
	BWS.current_tree_index = 0;
		
	BWS.screens = ['screen-loading','screen-choice','materi-utama','pelatihan-utama','video-utama','screen-instructions','screen-roots','screen-growing','screen-field','screen-errors'];
	BWS.elements = ['statistics','game-timer','navigation','factor-error','final-message'];

	// Hide all of the screens and make their visibility visibile
	$.each(BWS.screens, 
		function(index, value) {
			$('#' + value).css({visibility:'visible'}).hide();
		}
	);

	// Hide all of the elements and make their visibility visibile
	$.each(BWS.elements, 
		function(index, value) {
			$('#' + value).css({visibility:'visible'}).hide();
		}
	);

	// show the instructions screen
	BWS.show_screen('screen-instructions');
	
	// prefill the primes array and the available seeds array
	BWS.primes = BWS.get_primes(BWS.maximum_seed);
	BWS.seeds = BWS.get_seed_possible_values();

	// Add handlers to the buttons and other elements

	$('.factor-button').live('click',
		function() {
			BWS.setup_branch($(this).attr('id'));
		}
	);
	
	$('.smalltree').live(
		'mouseenter',
		function() {
			$(this).addClass('designable');
		}
	);
		
	$('.smalltree').live(
		'mouseleave',
		function() {
			$(this).removeClass('designable');
		}
	);
	
	$('#factor-answer-add').click(
		function() {
			var sep = ' x ';
			if ($("#factor-answer").html() == '') {
				sep = '';
			}
			var new_factor = '<input type="number" size="3" class="base-number"><input type="number" size="2" class="exponent-number">';
			$("#factor-answer").append(sep + new_factor);
		}
	);

	$('#check-factor-answer').click(
		function() {
			var val = BWS.parse_factor_answer();
			if (val == BWS.trees[BWS.current_tree_index].seed) {
				BWS.trees[BWS.current_tree_index].correct = 1;
				$('#trees-grown').text(BWS.get_correct_tree_count());
				if (BWS.game_mode == 'challenge') {
					BWS.show_screen('screen-field');
					BWS.add_tree_to_field(BWS.current_tree_index);
				} else {
					BWS.show_screen('screen-growing');
					BWS.grow_tree();
				}
			} else {
				// Fix me... put this alert inline next to the check button and have it disappear after a few seconds
				$('#factor-error').html('Faktorisasinya masih Kurang Tepat');
				$('#factor-error').show();
				BWS.ca_timeout = setTimeout(
					function() {
						$('#factor-error').fadeOut('slow');
					}
					,1500
				);
				BWS.trees[BWS.current_tree_index].correct = 0;
			}
		}
	);

	$('.print').click(
		function() {
			window.print();
			return false;
		}
	);
	
	$('.start-over').click( 
		function() {
			BWS.show_screen('screen-choice');
		}
	);

	$('.done-growing').click( 
		function() {
			BWS.show_screen('screen-field');
		}
	);

	$('.new-game').click( 
		function() {
			BWS.new_game();
		}
	);

	$('.done-planting').click( 
		function() {
			BWS.show_screen('screen-choice');
		}
	);

	$('.start-button').click(
		function() {
			if ($(this).text() == 'Tantangan Waktu') {
				BWS.game_mode = 'challenge';
			} else {
				BWS.game_mode = 'design';
			}
			BWS.start_game();
		}
	);
	$('.tombol-sp').click(
		function() {
			
			BWS.show_screen('materi-utama')
		}
	);
	$('.tombol-p').click(
		function() {
			
			BWS.show_screen('materi-utama')
		}
	);
	$('.tombol-c').click(
		function() {
			
			BWS.show_screen('materi-utama')
		}
	);
	$('.tombol-tp').click(
		function() {
			
			BWS.show_screen('materi-utama')
		}
	);
	$('.kembali').click(
		function() {
			
				BWS.show_screen('screen-instructions')
		}
	);
	$('.video').click(
		function() {
			
				BWS.show_screen('video-utama')
		}
	);

	$('.seed-button').click(

		function() {
		
			var new_tree = {};
			
			var button_text = $(this).text();

			new_tree.type = button_text.toLowerCase();
			new_tree.correct = 0;
			
			new_tree.seed = BWS.get_new_seed_value();
			
			if (new_tree.seed > 0) {
			
				//console.log(new_tree.seed);
			
				$('.factor-tree-branch').empty().removeClass('prime').removeClass('composite');
				$("#factor-answer").html('');
				$("#factor-answer-add").click();
	
				BWS.trees.push(new_tree);

				BWS.current_tree_index = BWS.trees.length - 1;

				BWS.show_screen('screen-roots');
				$('#rootseed-container').html('<span id="tree-seed-' + BWS.current_tree_index + '" class="rootseed ' + BWS.trees[BWS.current_tree_index].type + 'seed">' + BWS.uc_first(BWS.trees[BWS.current_tree_index].type) + '<' + '/span>');
				$('#factor').addClass('prime').html(BWS.trees[BWS.current_tree_index].seed + '<br><button class="factor-button" id="button-factor" title="Factor!">+<' + '/button>');
				
			} else {
			
				alert('No more seeds available! Taking you to the field.');
				BWS.end_game();
			
			}
		
		}
	);

}

BWS.get_primes = function(max_value) {

	if (max_value == undefined || isNaN(parseInt(max_value,10))) {
		max_value = 144;
	}

	var num = 0;
	var i = 0;
	var primes = [2];
	

	for (num = 3; num <= max_value; num++) {
	
		is_prime = true;
	
		for (i = 0; i < primes.length; i++) {
		
			if (num % primes[i] == 0) {
			
				is_prime = false;
				break;
			
			}
		
		}
		
		if (is_prime) {
			primes.push(num);
		}
	
	}
	
	return primes;

}

BWS.get_seed_possible_values = function() {

	var possible = [];

	for (var i = BWS.minimum_seed; i < BWS.maximum_seed; i++) {
	
		if (!BWS.array_search(i, BWS.primes)) {
		
			// it isn't prime. Check to see if any of its factors are too large
			
			good = 1;
			
			for (var j = 0; j < BWS.primes.length; j++) {
		
				if (i %  BWS.primes[j] == 0 && BWS.primes[j] > BWS.maximum_factor) {
				
					// this one won't work don't add it
					good = 0;
				
				}
			
			}
			
			if (good == 1) {
				
				// we have a good one, add it
			
				possible.push(i);
			
			}
		
		}
	
	}
	
	return possible;

}


BWS.get_new_seed_value = function() {

	// this function gets a seed value between the maximum and minimum values that
	// (a) is not prime and (b) has not yet been selected

	if (BWS.seeds.length == 0)  {
		
		return -1;
	
	} else {
	
		var n = BWS.random_number(0, (BWS.seeds.length - 1));

		var selected = BWS.seeds[n];
		
		BWS.seeds = BWS.remove_element(n, BWS.seeds);
	
		return selected;
	
	}

}

BWS.setup_branch = function(element_id) {

	var split = 0;
	var $button = $("#" + element_id);
	
	var element_base = element_id.replace(/^button-/,'');

	var $td_element = $("#" + element_base);
	
	if ($button.text() == '+') {
		split = 1;
		$button.text('-');
	} else {
		split = 0;
		$button.text('+');
	}
	
	if (split == 1) {
	
		// create the split items
		
		// input- + element_id

		var button_node = '<br><button class="factor-button" id="button-' + element_base  + 'TOKEN">+<' + '/button>';
		
		// console.log(element_base);
		
		if (element_base.match(/-\d-\d-\d-\d$/)) {
			button_node = '';
		}

		var node = '<input type="number" size="3" class="factor-tree-node" id="input-' + element_base + 'TOKEN" value="">' + button_node;
		var left_node = node.replace(/TOKEN/g,'-1');
		var right_node = node.replace(/TOKEN/g,'-2');
		
		$td_element.removeClass('prime').addClass('composite');
		
		$("#" + element_base + "-1").html(left_node).removeClass('prime').addClass('composite'); 
		$("#" + element_base + "-2").html(right_node).removeClass('prime').addClass('composite'); 
	
	} else {
	
		// Don't remove the class from the current branching one, because the branch is related to the previous one
		//$td_element.removeClass('composite').addClass('prime');
		
		for (var i = 1; i <= 2; i++) {
			$("#" + element_base + "-" + i).html('').removeClass('prime').removeClass('composite');
			for (var j = 1; j <= 2; j++) {
				$("#" + element_base + "-" + i + "-" + j).html('').removeClass('prime').removeClass('composite');
				for (var k = 1; k <= 2; k++) {
					$("#" + element_base + "-" + i + "-" + j + "-" + k).html('').removeClass('prime').removeClass('composite');
					for (var l = 1; l <= 2; l++) {
						$("#" + element_base + "-" + i + "-" + j + "-" + k + "-" + l).html('').removeClass('prime').removeClass('composite');
						for (var m = 1; m <= 2; m++) {
							$("#" + element_base + "-" + i + "-" + j + "-" + k + "-" + l + "-" + m).html('').removeClass('prime').removeClass('composite');
						}
					}
				}
			}
		}
	
	}
	
}

BWS.parse_factor_answer = function() {

	var val = 1;
	var i = 0;
	
	var base = 0;
	var exponenet = 0;
	
	var answer_parts = [];
	var used_bases = [];

	$("#factor-answer").find("input").each(
		function() {
			answer_parts.push($(this).val());
		}
	);
	
	// console.log(answer_parts);
	
	for (i = 0; i < answer_parts.length; i+=2) {
	
		base = answer_parts[i];
		exponent = answer_parts[(i+1)];
		
		if (exponent == '') {
			exponent = 1;
		}
		
		if (base != '') {
		
			// ensure that the base is a prime and hasn't been used yet 
			// we aren't currently searching the order as well, but we could with a simple > last in the used_bases

			if (BWS.array_search(base, BWS.primes) && !BWS.array_search(base,used_bases)) { 

				used_bases.push(base);	
				val *= Math.pow(base,exponent);
			
			}
		
		}
	
	}
	
	return val;

}

BWS.get_correct_tree_count = function() {

	var count = 0;
	
	for (var i = 0; i < BWS.trees.length; i++) {
	
		if (BWS.trees[i].correct == 1) {
			count++;
		}
	
	}
	
	return count;

}

BWS.grow_tree = function() {

	// This takes the current seed and grows it.
	
	$(".growing").remove();
	var this_tree = '<div id="growing-tree-' + BWS.current_tree_index + '" class="seed ' + BWS.trees[BWS.current_tree_index].type + 'seed growing">' + BWS.trees[BWS.current_tree_index].seed + '</div>';
	$("#screen-growing").append(this_tree);
	BWS.grow_tree_timeout = setTimeout(
		function() {
			// first swap the type of seed for the type of tree
			$('#growing-tree-' + BWS.current_tree_index).removeClass(BWS.trees[BWS.current_tree_index].type + 'seed').addClass(BWS.trees[BWS.current_tree_index].type + 'tree');
			// now animate the growing of the seed into the tree
			$('#growing-tree-' + BWS.current_tree_index).switchClass('seed','tree',BWS.tree_growth_time*1000,'swing',
				function(){
					BWS.show_field_timeout = setTimeout(
						function() {
							BWS.show_screen('screen-field');
							BWS.add_tree_to_field(BWS.current_tree_index);
						}
						, BWS.tree_display_time*1000
					);
				}
			);
		}
		, BWS.seed_gestation*1000
	);

}

BWS.add_tree_to_field = function(tree_index) {

	var this_tree = '<div id="grown-tree-' + tree_index + '" class="smalltree ' + BWS.trees[tree_index].type + 'smalltree grown"><img src="images/' + BWS.trees[tree_index].type + '.gif" alt="' + BWS.trees[tree_index].type + '"><br><span class="tree-seed">' + BWS.trees[tree_index].seed + '<' + '/span><' + '/div>';
	$("#screen-field").append(this_tree);
	if (BWS.game_mode == 'challenge') {
		$("#grown-tree-" + tree_index).css('left',(60 + (tree_index * 50)) + 'px').css('bottom','60px');
	} else {
		$("#grown-tree-" + tree_index).draggable({containment:'#screen-field',stack:'#screen-field .smalltree'}).resizable({handles:"all",containment:'#screen-field',aspectRatio: true}).css('position','absolute');
	}

}


// This is the main application code

BWS.new_game = function() {

	if (BWS.game_over == 1 || confirm('Anda yakin ingin mengulang?')) {
		clearTimeout(BWS.game_timeout);
		BWS.game_over = 1;
		BWS.show_screen('screen-instructions');
	}

}

BWS.start_game = function() {

	BWS.game_over = 0;
	
	$('#screen-field .grown').remove();
	$("#final-message").html('');
	
	if (BWS.game_mode == 'challenge') {
		$('#screen-field').removeClass().addClass('challenge');
		BWS.game_time = BWS.time_limit * 60;
		BWS.update_game_timer();
	} else {
		$('#screen-field').removeClass().addClass('design');
		$('#timer').html('Tak Terbatas');
	}
	//lompat
	BWS.show_screen('screen-choice');
	//BWS.show_screen('screen-roots');
	$(".done-planting").show();

	// The trees that a user creates
	BWS.trees = [];
	BWS.current_tree_index = 0;

}

BWS.show_screen = function(screen_id) {

	$.each(BWS.screens, 
		function(index, value) {
			if (value != screen_id) {
				$('#' + value).hide();
			}
		}
	);
	
	if (screen_id == 'screen-instructions') {
		$('#navigation').hide();
		$('#statistics').hide();
		$('#game-timer').hide();
		
	} else {
		$('#navigation').show();
		$('#statistics').show();
		$('#game-timer').show();
		 
	}

	$('#' + screen_id).show('fast');
	
}

BWS.end_game = function() {

	BWS.game_over = 1;
	clearTimeout(BWS.game_timeout);

	BWS.show_screen('screen-field');

	$(".done-planting").hide();
	
	var correct_count = BWS.get_correct_tree_count();
	var final_message = '';
	
	if (correct_count <= 0) {
		final_message = '<h1>Better luck next time<' + '/h1><p>You didn\'t grow any trees this time, but you\'ll surely do better next time.<' + '/p>';
	} else if (correct_count == 1) {
		final_message = '<h1>A good start<' + '/h1><p>You grew one tree. Keep trying to grow more.<' + '/p>';
	} else if (correct_count < 10) {
		final_message = '<h1>Good Job<' + '/h1><p>You did a good job growing your <strong>' + correct_count + '<' + '/strong> trees.<' + '/p>';
	} else {
		final_message = '<h1>Fantastic!<' + '/h1><p>You did a truly excellent job growing your forest of <strong>' + correct_count + '<' + '/strong> trees. Nice work!<' + '/p>';
	}

	$("#final-message").html(final_message).show('slow');

}
